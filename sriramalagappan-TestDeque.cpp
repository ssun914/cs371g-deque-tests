// -------------
// TestDeque.c++
// -------------

// http://www.cplusplus.com/reference/deque/deque/

// --------
// includes
// --------

#include <deque>     // deque
#include <stdexcept> // invalid_argument, out_of_range

#include "gtest/gtest.h"

#include "Deque.hpp"

// -----
// Using
// -----

using namespace std;
using namespace testing;

// -----
// Types
// -----

template <typename T>
struct DequeFixture : Test {
    using deque_type    = T;
    using allocator_type = typename deque_type::allocator_type;
    using iterator       = typename deque_type::iterator;
    using const_iterator = typename deque_type::const_iterator;
};

using
deque_types =
    Types<
    deque<int>,
    my_deque<int>,
    deque<int, allocator<int>>,
    my_deque<int, allocator<int>>>;

#ifdef __APPLE__
TYPED_TEST_CASE(DequeFixture, deque_types,);
#else
TYPED_TEST_CASE(DequeFixture, deque_types);
#endif

// -----
// Tests
// -----

TYPED_TEST(DequeFixture, equal0) {
    using deque_type = typename TestFixture::deque_type;
    const deque_type x = {3, 7, 1};
    deque_type y = {3, 7, 1};
    ASSERT_TRUE(x == y);
}

TYPED_TEST(DequeFixture, equal1) {
    using deque_type = typename TestFixture::deque_type;
    const deque_type x = {3, 7, 1};
    deque_type y = {3, 7, 2};
    ASSERT_TRUE(!(x == y));
}

TYPED_TEST(DequeFixture, equal2) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x, y;
    ASSERT_TRUE(x == y);
}

TYPED_TEST(DequeFixture, equal3) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x, y;
    x.push_back(37);
    y.push_front(37);
    x.push_back(1);
    y.push_back(1);
    ASSERT_TRUE(x == y);
}

TYPED_TEST(DequeFixture, not_equal0) {
    using deque_type = typename TestFixture::deque_type;
    const deque_type x = {3, 7, 1};
    deque_type y = {3, 7, 2};
    ASSERT_TRUE(x != y);
}

TYPED_TEST(DequeFixture, not_equal1) {
    using deque_type = typename TestFixture::deque_type;
    const deque_type x = {3, 7, 1};
    deque_type y = {3, 7, 1};
    ASSERT_TRUE(!(x != y));
}

TYPED_TEST(DequeFixture, not_equal2) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x, y;
    x.push_back(3);
    y.push_front(3);
    x.push_back(1);
    y.push_front(1);
    ASSERT_TRUE(x != y);
}

TYPED_TEST(DequeFixture, less_than0) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x = {3, 7, 1};
    deque_type y = {3, 7, 2};
    ASSERT_TRUE(x < y);
}

// TYPED_TEST(DequeFixture, less_than1) {
//     using deque_type = typename TestFixture::deque_type;
//     const deque_type x = {3, 7, 1};
//     const deque_type y = {3, 7, 1};
//     ASSERT_TRUE(!(x < y));
// }

// TYPED_TEST(DequeFixture, less_than2) {
//     using deque_type = typename TestFixture::deque_type;
//     const deque_type x = {3, 7, 1};
//     const deque_type y = {3, 7, 0};
//     ASSERT_TRUE(!(x < y));
// }

// TYPED_TEST(DequeFixture, less_than3) {
//     using deque_type = typename TestFixture::deque_type;
//     deque_type x = {3, 7, 1};
//     deque_type y = {3, 7, 0};
//     x.push_back(-3);
//     ASSERT_TRUE(!(x < y));
// }

// TYPED_TEST(DequeFixture, less_than4) {
//     using deque_type = typename TestFixture::deque_type;
//     deque_type x = {3, 7, 1};
//     deque_type y = {3, 7, 0};
//     x.push_back(3);
//     ASSERT_TRUE(!(x < y));
// }

// TYPED_TEST(DequeFixture, less_than5) {
//     using deque_type = typename TestFixture::deque_type;
//     const deque_type x = {3, 7, -1};
//     const deque_type y = {3, 7, 0};
//     ASSERT_TRUE(x < y);
// }

TYPED_TEST(DequeFixture, greater_than0) {
    using deque_type = typename TestFixture::deque_type;
    const deque_type x = {3, 7, 1};
    const deque_type y = {3, 7, 0};
    ASSERT_TRUE(x > y);
}

// TYPED_TEST(DequeFixture, greater_than1) {
//     using deque_type = typename TestFixture::deque_type;
//     deque_type x = {3, 7, 1};
//     deque_type y = {3, 7, 2};
//     ASSERT_TRUE(!(x > y));
// }

// TYPED_TEST(DequeFixture, greater_than2) {
//     using deque_type = typename TestFixture::deque_type;
//     const deque_type x = {3, 7, 1};
//     const deque_type y = {3, 7, 1};
//     ASSERT_TRUE(!(x > y));
// }

// TYPED_TEST(DequeFixture, greater_than3) {
//     using deque_type = typename TestFixture::deque_type;
//     deque_type x = {3, 7, 1};
//     deque_type y = {3, 7, 0};
//     x.push_back(-3);
//     ASSERT_TRUE(x > y);
// }

// TYPED_TEST(DequeFixture, greater_than4) {
//     using deque_type = typename TestFixture::deque_type;
//     deque_type x = {3, 7, 1};
//     deque_type y = {3, 7, 0};
//     x.push_back(3);
//     ASSERT_TRUE(x > y);
// }

// TYPED_TEST(DequeFixture, greater_than5) {
//     using deque_type = typename TestFixture::deque_type;
//     const deque_type x = {3, 7, -1};
//     const deque_type y = {3, 7, 0};
//     ASSERT_TRUE(!(x > y));
// }

TYPED_TEST(DequeFixture, less_than_or_equal_to0) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x = {3, 7, 1};
    deque_type y = {3, 7, 2};
    ASSERT_TRUE(x <= y);
}

// TYPED_TEST(DequeFixture, less_than_or_equal_to1) {
//     using deque_type = typename TestFixture::deque_type;
//     const deque_type x = {3, 7, 1};
//     const deque_type y = {3, 7, 1};
//     ASSERT_TRUE(x <= y);
// }

// TYPED_TEST(DequeFixture, less_than_or_equal_to2) {
//     using deque_type = typename TestFixture::deque_type;
//     const deque_type x = {3, 7, 1};
//     const deque_type y = {3, 7, 0};
//     ASSERT_TRUE(!(x <= y));
// }

// TYPED_TEST(DequeFixture, less_than_or_equal_to3) {
//     using deque_type = typename TestFixture::deque_type;
//     deque_type x = {3, 7, 1};
//     deque_type y = {3, 7, 0};
//     x.push_back(-3);
//     ASSERT_TRUE(!(x <= y));
// }

// TYPED_TEST(DequeFixture, less_than_or_equal_to4) {
//     using deque_type = typename TestFixture::deque_type;
//     deque_type x = {3, 7, 1};
//     deque_type y = {3, 7, 0};
//     x.push_back(3);
//     ASSERT_TRUE(!(x <= y));
// }

// TYPED_TEST(DequeFixture, less_than_or_equal_to5) {
//     using deque_type = typename TestFixture::deque_type;
//     const deque_type x = {3, 7, -1};
//     const deque_type y = {3, 7, 0};
//     ASSERT_TRUE(x <= y);
// }

TYPED_TEST(DequeFixture, greater_than_or_equal_to0) {
    using deque_type = typename TestFixture::deque_type;
    const deque_type x = {3, 7, 1};
    const deque_type y = {3, 7, 0};
    ASSERT_TRUE(x >= y);
}

// TYPED_TEST(DequeFixture, greater_than_or_equal_to1) {
//     using deque_type = typename TestFixture::deque_type;
//     deque_type x = {3, 7, 1};
//     deque_type y = {3, 7, 2};
//     ASSERT_TRUE(!(x >= y));
// }

// TYPED_TEST(DequeFixture, greater_than_or_equal_to2) {
//     using deque_type = typename TestFixture::deque_type;
//     const deque_type x = {3, 7, 1};
//     const deque_type y = {3, 7, 1};
//     ASSERT_TRUE(x >= y);
// }

// TYPED_TEST(DequeFixture, greater_than_or_equal_to3) {
//     using deque_type = typename TestFixture::deque_type;
//     deque_type x = {3, 7, 1};
//     deque_type y = {3, 7, 0};
//     x.push_back(-3);
//     ASSERT_TRUE(x >= y);
// }

// TYPED_TEST(DequeFixture, greater_than_or_equal_to4) {
//     using deque_type = typename TestFixture::deque_type;
//     deque_type x = {3, 7, 1};
//     deque_type y = {3, 7, 0};
//     x.push_back(3);
//     ASSERT_TRUE(x >= y);
// }

// TYPED_TEST(DequeFixture, greater_than_or_equal_to5) {
//     using deque_type = typename TestFixture::deque_type;
//     const deque_type x = {3, 7, -1};
//     const deque_type y = {3, 7, 0};
//     ASSERT_TRUE(!(x >= y));
// }

TYPED_TEST(DequeFixture, swap) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x = {3, 7, 1, 2};
    deque_type y = {314, 429, 371};
    x.swap(y);
    deque_type new_x = {314, 429, 371};
    deque_type new_y = {3, 7, 1, 2};
    ASSERT_TRUE((x == new_x) && (y == new_y));
}

TYPED_TEST(DequeFixture, default_constructor) {
    using deque_type = typename TestFixture::deque_type;
    const deque_type x;
    ASSERT_TRUE(x.empty());
    ASSERT_EQ(x.size(), 0u);
}

TYPED_TEST(DequeFixture, size_constructor) {
    using deque_type = typename TestFixture::deque_type;
    deque_type actual (5);
    deque_type expected = {0, 0, 0, 0, 0};
    ASSERT_TRUE(actual.size() == 5);
    ASSERT_EQ(actual, expected);
}

TYPED_TEST(DequeFixture, fill_constructor) {
    using deque_type = typename TestFixture::deque_type;
    deque_type actual (3, 371);
    deque_type expected = {371, 371, 371};
    ASSERT_TRUE(actual.size() == 3);
    ASSERT_EQ(actual, expected);
}

TYPED_TEST(DequeFixture, initializer_list_constructor) {
    using deque_type = typename TestFixture::deque_type;
    deque_type actual {3, 10, 7};
    deque_type expected = {3, 10, 7};
    ASSERT_TRUE(actual.size() == 3);
    ASSERT_EQ(actual, expected);
}

TYPED_TEST(DequeFixture, copy_constructor) {
    using deque_type = typename TestFixture::deque_type;
    deque_type expected = {3, 10, 7};
    deque_type actual (expected);
    ASSERT_TRUE(actual.size() == 3);
    ASSERT_EQ(actual, expected);
}

TYPED_TEST(DequeFixture, equal_operator) {
    using deque_type = typename TestFixture::deque_type;
    deque_type expected = {3, 10, 7};
    deque_type actual = {17, 5638, 1984, 0, 327};
    actual = expected;
    ASSERT_TRUE(actual.size() == 3);
    ASSERT_EQ(actual, expected);
}

TYPED_TEST(DequeFixture, index_operator) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x = {17, 5638, -1984, 0, 327, -81, 922, -412, 210, 7};
    int actual_0 = x[0];
    int actual_1 = x[7];
    int actual_2 = x[9];
    ASSERT_EQ(actual_0, 17);
    ASSERT_EQ(actual_1, -412);
    ASSERT_EQ(actual_2, 7);
}

TYPED_TEST(DequeFixture, const_index_operator) {
    using deque_type = typename TestFixture::deque_type;
    const deque_type x = {1126, -52238, 1984, 18, 327, -81, 958, 4142, 216, -7};
    const int actual_0 = x[0];
    const int actual_1 = x[7];
    const int actual_2 = x[9];
    ASSERT_EQ(actual_0, 1126);
    ASSERT_EQ(actual_1, 4142);
    ASSERT_EQ(actual_2, -7);
}

TYPED_TEST(DequeFixture, at) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x = {1126, -52238, 1984, 18, 327, -81, 958, 4142, 216, -7};
    int actual_0 = x.at(0);
    int actual_1 = x.at(7);
    int actual_2 = x.at(9);
    ASSERT_EQ(actual_0, 1126);
    ASSERT_EQ(actual_1, 4142);
    ASSERT_EQ(actual_2, -7);
}

TYPED_TEST(DequeFixture, const_at) {
    using deque_type = typename TestFixture::deque_type;
    const deque_type x = {1126, -52238, 1984, 18, 327, -81, 958, 4142, 216, -7};
    const int actual_0 = x.at(0);
    const int actual_1 = x.at(7);
    const int actual_2 = x.at(9);
    ASSERT_EQ(actual_0, 1126);
    ASSERT_EQ(actual_1, 4142);
    ASSERT_EQ(actual_2, -7);
}

TYPED_TEST(DequeFixture, back) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x = {3, 7, 1};
    int actual = x.back();
    ASSERT_EQ(actual, 1);
}

TYPED_TEST(DequeFixture, const_back) {
    using deque_type = typename TestFixture::deque_type;
    const deque_type x = {3, 7, 1};
    const int actual = x.back();
    ASSERT_EQ(actual, 1);
}

TYPED_TEST(DequeFixture, front) {
    using deque_type = typename TestFixture::deque_type;
    const deque_type x = {3, 7, 1};
    const int actual = x.front();
    ASSERT_EQ(actual, 3);
}

TYPED_TEST(DequeFixture, const_front) {
    using deque_type = typename TestFixture::deque_type;
    const deque_type x = {3, 7, 1};
    const int actual = x.front();
    ASSERT_EQ(actual, 3);
}

TYPED_TEST(DequeFixture, begin) {
    using deque_type = typename TestFixture::deque_type;
    using iterator = typename TestFixture::iterator;

    deque_type x = {3, 7, 1};
    iterator b = begin(x);
    int actual = *b;
    ASSERT_EQ(actual, 3);
}

TYPED_TEST(DequeFixture, const_begin) {
    using deque_type = typename TestFixture::deque_type;
    using const_iterator = typename TestFixture::const_iterator;
    const deque_type x = {3, 7, 1};
    const_iterator b = begin(x);
    const int actual = *b;
    ASSERT_EQ(actual, 3);
}

TYPED_TEST(DequeFixture, end) {
    using deque_type = typename TestFixture::deque_type;
    using iterator = typename TestFixture::iterator;
    deque_type x = {3, 7, 1};
    iterator e = end(x);
    --e;
    int actual = *e;
    ASSERT_EQ(actual, 1);
}

TYPED_TEST(DequeFixture, const_end) {
    using deque_type = typename TestFixture::deque_type;
    using const_iterator = typename TestFixture::const_iterator;
    const deque_type x = {3, 7, 1};
    const_iterator e = end(x);
    --e;
    const int actual = *e;
    ASSERT_EQ(actual, 1);
}

TYPED_TEST(DequeFixture, clear) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x = {3, 7, 1};
    x.clear();
    ASSERT_TRUE(x.empty());
    ASSERT_EQ(x.size(), 0u);
}

TYPED_TEST(DequeFixture, empty) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x = {3, 7, 1};
    deque_type y;
    ASSERT_TRUE(!(x.empty()));
    ASSERT_TRUE(y.empty());
}

TYPED_TEST(DequeFixture, erase0) {
    using deque_type = typename TestFixture::deque_type;
    using iterator = typename TestFixture::iterator;
    deque_type x = {3, 1, 4, 4, 2, 9, 3, 7, 1};
    iterator pos = begin(x);

    ++++++pos;
    x.erase(pos);

    deque_type expected = {3, 1, 4, 2, 9, 3, 7, 1};

    ASSERT_TRUE(x.size() == 8);
    ASSERT_EQ(x, expected);
}

TYPED_TEST(DequeFixture, erase1) {
    using deque_type = typename TestFixture::deque_type;
    using iterator = typename TestFixture::iterator;
    deque_type x = {3, 1, 4, 4, 2, 9, 3, 7, 1};
    iterator pos = end(x);

    ------pos;
    x.erase(pos);

    deque_type expected = {3, 1, 4, 4, 2, 9, 7, 1};

    ASSERT_TRUE(x.size() == 8);
    ASSERT_EQ(x, expected);
}

TYPED_TEST(DequeFixture, insert0) {
    using deque_type = typename TestFixture::deque_type;
    using iterator = typename TestFixture::iterator;
    deque_type x = {3, 7};
    iterator pos = end(x);

    x.insert(pos, 1);

    deque_type expected = {3, 7, 1};

    ASSERT_TRUE(x.size() == 3);
    ASSERT_EQ(x, expected);
}

TYPED_TEST(DequeFixture, insert1) {
    using deque_type = typename TestFixture::deque_type;
    using iterator = typename TestFixture::iterator;
    deque_type x = {7, 1};
    iterator pos = begin(x);

    x.insert(pos, 3);

    deque_type expected = {3, 7, 1};

    ASSERT_TRUE(x.size() == 3);
    ASSERT_EQ(x, expected);
}

TYPED_TEST(DequeFixture, insert2) {
    using deque_type = typename TestFixture::deque_type;
    using iterator = typename TestFixture::iterator;
    deque_type x = {7, 1};
    iterator pos = end(x);
    pos -= 1;

    x.insert(pos, 3);

    deque_type expected = {7, 3, 1};

    ASSERT_TRUE(x.size() == 3);
    ASSERT_EQ(x, expected);
}

TYPED_TEST(DequeFixture, pop_back) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x = {3, 7, 1};

    x.pop_back();

    deque_type expected = {3, 7};

    ASSERT_TRUE(x.size() == 2);
    ASSERT_EQ(x, expected);
}

TYPED_TEST(DequeFixture, pop_front) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x = {3, 7, 1};

    x.pop_front();

    deque_type expected = {7, 1};

    ASSERT_TRUE(x.size() == 2);
    ASSERT_EQ(x, expected);
}

TYPED_TEST(DequeFixture, push_back) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x = {3, 7};

    x.push_back(1);

    deque_type expected = {3, 7, 1};

    ASSERT_TRUE(x.size() == 3);
    ASSERT_EQ(x, expected);
}

TYPED_TEST(DequeFixture, push_front) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x = {7, 1};

    x.push_front(3);

    deque_type expected = {3, 7, 1};

    ASSERT_TRUE(x.size() == 3);
    ASSERT_EQ(x, expected);
}

TYPED_TEST(DequeFixture, resize0) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x = {3, 1, 4, 4, 2, 9, 3, 7, 1};

    x.resize(3);

    deque_type expected = {3, 1, 4};

    ASSERT_TRUE(x.size() == 3);
    ASSERT_EQ(x, expected);
}

TYPED_TEST(DequeFixture, resize1) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x = {3, 1, 4, 4, 2, 9, 3, 7, 1};

    x.resize(0);

    deque_type expected = {};

    ASSERT_TRUE(x.size() == 0);
    ASSERT_TRUE(x.empty());
    ASSERT_EQ(x, expected);
}

TYPED_TEST(DequeFixture, resize2) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x = {3, 1, 4, 4, 2, 9, 3, 7, 1};

    x.resize(12);

    deque_type expected = {3, 1, 4, 4, 2, 9, 3, 7, 1, 0, 0, 0};

    ASSERT_TRUE(x.size() == 12);
    ASSERT_EQ(x, expected);
}

TYPED_TEST(DequeFixture, resize3) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x = {3, 1, 4, 4, 2, 9, 3, 7, 1};

    x.resize(12, 8);

    deque_type expected = {3, 1, 4, 4, 2, 9, 3, 7, 1, 8, 8, 8};

    ASSERT_TRUE(x.size() == 12);
    ASSERT_EQ(x, expected);
}

TYPED_TEST(DequeFixture, resize4) {
    using deque_type = typename TestFixture::deque_type;
    deque_type x = {3, 1, 4, 4, 2, 9, 3, 7, 1};

    x.resize(4, 8);

    deque_type expected = {3, 1, 4, 4};

    ASSERT_TRUE(x.size() == 4);
    ASSERT_EQ(x, expected);
}

TYPED_TEST(DequeFixture, equal_iterator) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x;
    iterator b = begin(x);
    iterator e = end(x);

    ASSERT_TRUE(b == e);
}

TYPED_TEST(DequeFixture, const_equal_iterator) {
    using deque_type     = typename TestFixture::deque_type;
    using const_iterator = typename TestFixture::const_iterator;
    const deque_type x;
    const_iterator b = begin(x);
    const_iterator e = end(x);
    ASSERT_TRUE(b == e);
}
