// -------------
// TestDeque.c++
// -------------

// http://www.cplusplus.com/reference/deque/deque/

// --------
// includes
// --------

#include <deque>     // deque
#include <stdexcept> // invalid_argument, out_of_range
#include <iostream>

#include "gtest/gtest.h"

#include "Deque.hpp"

// -----
// Using
// -----

using namespace std;
using namespace testing;

// -----
// Types
// -----

template <typename T>
struct DequeFixture : Test {
    using deque_type    = T;
    using allocator_type = typename deque_type::allocator_type;
    using iterator       = typename deque_type::iterator;
    using const_iterator = typename deque_type::const_iterator;
};

using
deque_types =
    Types<
    deque<int>,
    my_deque<int>,
    deque<int, allocator<int>>,
    my_deque<int, allocator<int>>
    >;

#ifdef __APPLE__
TYPED_TEST_CASE(DequeFixture, deque_types,);
#else
TYPED_TEST_CASE(DequeFixture, deque_types);
#endif

// -----
// Tests
// -----

TYPED_TEST(DequeFixture, test1) {
    using deque_type = typename TestFixture::deque_type;
    const deque_type x;
    ASSERT_TRUE(x.empty());
    EXPECT_TRUE(x.size() == 0u);}

TYPED_TEST(DequeFixture, test2) {
    using deque_type = typename TestFixture::deque_type;
    using iterator   = typename TestFixture::iterator;
    deque_type x;
    iterator b = begin(x);
    iterator e = end(x);
    EXPECT_TRUE(b == e);}

TYPED_TEST(DequeFixture, test3) {
    using deque_type     = typename TestFixture::deque_type;
    using const_iterator = typename TestFixture::const_iterator;
    const deque_type x;
    const_iterator b = begin(x);
    const_iterator e = end(x);
    EXPECT_TRUE(b == e);}

/*
 * TESTING DEFAULT CONSTRUCTOR
 */



TYPED_TEST(DequeFixture, test_for_default_constructor) {
    using deque_type = typename TestFixture::deque_type;
    const deque_type x(1);
    ASSERT_FALSE(x.empty());
    EXPECT_EQ(x.size(), 1);
}

/*
 * TESTING ITERATOR == OPERATOR
 */
TYPED_TEST(DequeFixture, test1_for_iterator_EQUAL_OPERATOR) {
    using deque_type     = typename TestFixture::deque_type;
    using iterator = typename TestFixture::iterator;
    deque_type x(20);
    iterator b = begin(x);
    iterator e = end(x);
    EXPECT_TRUE(b != e);
}

TYPED_TEST(DequeFixture, test2_for_iterator_EQUAL_OPERATOR) {
    using deque_type     = typename TestFixture::deque_type;
    using iterator = typename TestFixture::iterator;
    deque_type x(20);
    iterator b1 = begin(x);
    iterator e1 = end(x);
    iterator b2 = begin(x);
    iterator e2 = end(x);
    EXPECT_TRUE(b1 == b2 && e1== e2);
}

/*
 * TESTING ITERATOR * OPERATOR
 */
TYPED_TEST(DequeFixture, test1_for_iterator_POINTER) {
    using deque_type     = typename TestFixture::deque_type;
    using iterator = typename TestFixture::iterator;
    //deque_type x;
    // for(int i = 0; i < 30; i++){
    //     x.push_back(i);
    // }
    deque_type x(30,3);
    iterator e = end(x);
    iterator b = begin(x);
    e--;
    bool pass = true;
    int i = 29;
    while(e != b) {
        if(x[i] != *e) {
            pass = false;
        }
        i--;
        e--;
    }
    EXPECT_TRUE(pass);
}

TYPED_TEST(DequeFixture, test2_for_iterator_POINTER) {
    using deque_type     = typename TestFixture::deque_type;
    using iterator = typename TestFixture::iterator;
    // deque_type x;
    // for(int i = 0; i < 30; i++){
    //     x.push_back(i);
    // }
    deque_type x(20, 1);
    iterator b = begin(x);
    b++;
    *b = 123;
    EXPECT_TRUE(x[1] == 123);
}

/*
 * TESTING ITERATOR ++ OPERATOR
 */
TYPED_TEST(DequeFixture, test1_for_iterator_INCREMENT) {
    using deque_type     = typename TestFixture::deque_type;
    using iterator = typename TestFixture::iterator;
    deque_type x;
    for(int i = 0; i < 4; i++) {
        x.push_back(i);
    }
    iterator b1 = begin(x);
    b1++;
    EXPECT_TRUE(*b1 == x[1]);
}

TYPED_TEST(DequeFixture, test2_for_iterator_INCREMENT) {
    using deque_type     = typename TestFixture::deque_type;
    using iterator = typename TestFixture::iterator;
    deque_type x({0,1,2,3,5,8,13,21,34,66,66});
    iterator b = begin(x);
    iterator e = end(x);
    bool pass = true;
    int i = 0;
    while(b != e) {
        if(*b != x[i])
            pass = false;
        b++;
        i++;
    }
    EXPECT_TRUE(pass == true);
}

/*
 * TESTING ITERATOR += OPERATOR
 */
TYPED_TEST(DequeFixture, test_for_iterator_PLUS_BY) {
    using deque_type     = typename TestFixture::deque_type;
    using iterator = typename TestFixture::iterator;
    // deque_type x;
    // for(int i = 0; i < 30; i++){
    //     x.push_back(i);
    // }
    deque_type x (30, 3);
    iterator b1 = begin(x);
    b1+=15;
    EXPECT_TRUE(*b1 == x[15]);
}

/*
 * TESTING ITERATOR -- OPERATOR
 */
TYPED_TEST(DequeFixture, test_for_iterator_DECREMENT) {
    using deque_type     = typename TestFixture::deque_type;
    using iterator = typename TestFixture::iterator;
    deque_type x;
    for(int i = 0; i < 4; i++) {
        x.push_back(i);
    }
    iterator e = end(x);
    e--;
    EXPECT_TRUE(*e == x[3]);
}

/*
 * TESTING ITERATOR -= OPERATOR
 */
TYPED_TEST(DequeFixture, test_for_iterator_MINUS_BY) {
    using deque_type     = typename TestFixture::deque_type;
    using iterator = typename TestFixture::iterator;
    // deque_type x;
    // for(int i = 0; i < 30; i++){
    //     x.push_back(i);
    // }
    deque_type x(30,3);
    iterator e1 = end(x);
    e1-=15;
    EXPECT_TRUE(*e1 == x[15]);
}


/*
 * TESTING CONST_ITERATOR == OPERATOR
 */
TYPED_TEST(DequeFixture, test1_for_const_iterator_EQUAL_OPERATOR) {
    using deque_type     = typename TestFixture::deque_type;
    using const_iterator = typename TestFixture::const_iterator;
    const deque_type x(20);
    const_iterator b = begin(x);
    const_iterator e = end(x);
    EXPECT_TRUE(b != e);
}

TYPED_TEST(DequeFixture, test2_for_const_iterator_EQUAL_OPERATOR) {
    using deque_type     = typename TestFixture::deque_type;
    using const_iterator = typename TestFixture::const_iterator;
    const deque_type x(20);
    const_iterator b1 = begin(x);
    const_iterator e1 = end(x);
    const_iterator b2 = begin(x);
    const_iterator e2 = end(x);
    EXPECT_TRUE(b1 == b2 && e1== e2);
}

/*
 * TESTING  CONST_ITERATOR * OPERATOR
 */
TYPED_TEST(DequeFixture, test_for_const_iterator_POINTER) {
    using deque_type     = typename TestFixture::deque_type;
    using const_iterator = typename TestFixture::const_iterator;
    const deque_type x({0,1,2,3,5,8,16});
    const_iterator b = begin(x);
    EXPECT_TRUE(*b == 0);
}

/*
 * TESTING CONST_ITERATOR ++ OPERATOR
 */
TYPED_TEST(DequeFixture, test1_for_const_iterator_INCREMENT) {
    using deque_type     = typename TestFixture::deque_type;
    using const_iterator = typename TestFixture::const_iterator;
    const deque_type x({0,1,2,3,5,8,16});
    const_iterator b = begin(x);
    b++;
    EXPECT_TRUE(*b == 1);
}

TYPED_TEST(DequeFixture, test2_for_const_iterator_INCREMENT) {
    using deque_type     = typename TestFixture::deque_type;
    using const_iterator = typename TestFixture::const_iterator;
    const deque_type x({0,1,2,3,5,8,13,21,34});
    const_iterator b = begin(x);
    const_iterator e = end(x);
    bool pass = true;
    int i = 0;
    while(b != e) {
        if(*b != x[i])
            pass = false;
        b++;
        i++;
    }
    EXPECT_TRUE(pass == true);
}

/*
 * TESTING CONST_ITERATOR += OPERATOR
 */
TYPED_TEST(DequeFixture, test_for_const_iterator_PLUS_BY) {
    using deque_type     = typename TestFixture::deque_type;
    using const_iterator = typename TestFixture::const_iterator;
    const deque_type x({0,1,2,3,5,8,16});
    const_iterator b = begin(x);
    b+=2;
    EXPECT_TRUE(*b == 2);
}

/*
 * TESTING CONST_ITERATOR -- OPERATOR
 */
TYPED_TEST(DequeFixture, test_for_const_iterator_DECREMENT) {
    using deque_type     = typename TestFixture::deque_type;
    using const_iterator = typename TestFixture::const_iterator;
    const deque_type x({0,1,2,3,5,8,16});
    const_iterator e = end(x);
    e--;
    EXPECT_TRUE(*e == 16);
}

/*
 * TESTING CONST_ITERATOR -= OPERATOR
 */
TYPED_TEST(DequeFixture, test_for_const_iterator_MINUS_BY) {
    using deque_type     = typename TestFixture::deque_type;
    using const_iterator = typename TestFixture::const_iterator;
    const deque_type x({0,1,2,3,5,8,16});
    const_iterator e = end(x);
    e-=2;
    EXPECT_TRUE(*e == 8);
}

/*
 * TESTING COPY CONSTRUCTOR
 */
TYPED_TEST(DequeFixture, test_COPY_constructor) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x({0,1,2,3,5,8,16});
    deque_type a(x);
    bool pass = true;
    int length = x.size();
    for(int i = 0; i < length; i++) {
        if(x[i] != a[i])
            pass = false;
    }
    EXPECT_TRUE(pass);
}

/*
 * TESTING [] OPERATOR FOR DEQUE
 */
TYPED_TEST(DequeFixture, test_for_deque_bracket_operator) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x({0,1,2,3,5,8,16});
    int length = x.size();
    for(int j = 0; j < length; j++) {
        x[j] = j;
    }
    bool pass = true;
    for(int j = 0; j < length; j++) {
        if(x[j] != j)
            pass = false;
    }

    EXPECT_TRUE(pass);
}

/*
 * TESTING 'AT' FOR DEQUE
 */
TYPED_TEST(DequeFixture, test_for_deque_AT) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x({0,1,2,3,5,8,16});
    int length = x.size();
    for(int j = 0; j < length; j++) {
        x[j] = j;
    }
    bool pass = true;
    for(int j = 0; j < length; j++) {
        if(x.at(j) != j)
            pass = false;
    }

    EXPECT_TRUE(pass);
}

/*
 * TESTING 'BACK' FOR DEQUE
 */
TYPED_TEST(DequeFixture, test1_for_deque_BACK) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x({0,1,2,3,5,8,16});
    EXPECT_TRUE(x.back() == 16);
}

TYPED_TEST(DequeFixture, test2_for_deque_BACK) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x;
    for(int i = 0; i < 10; i++) {
        x.push_back(i);
    }
    EXPECT_TRUE(x.back() == 9);
}

/*
 * TESTING 'FRONT' FOR DEQUE
 */
TYPED_TEST(DequeFixture, test1_for_deque_FRONT) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x({0,1,2,3,5,8,16});
    EXPECT_TRUE(x.front() == 0);
}

TYPED_TEST(DequeFixture, test2_for_deque_FRONT) {
    using deque_type     = typename TestFixture::deque_type;
    using iterator       = typename TestFixture::iterator;
    deque_type x({0,1,2,3,4,5,6,7,8,9,10,11,12});
    // for(int i = 0; i < 20; i++) {
    //     x.push_back(i);
    // }
    iterator b = x.begin();
    iterator e = x.end();
    while(b != e){
        b++;
    }
    EXPECT_TRUE(x.front() == 0);
}

/*
 * TESTING 'INSERT' FOR DEQUE
 */
TYPED_TEST(DequeFixture, test1_for_deque_INSERT) {
    using deque_type     = typename TestFixture::deque_type;
    using iterator       = typename TestFixture::iterator;
    deque_type x(100);
    for(int i = 0; i < 100; i++) {
        x[i] = i;
    }
    iterator b = x.begin();
    x.insert(b,-1);
    EXPECT_TRUE(x.front() == -1);
}

TYPED_TEST(DequeFixture, test2_for_deque_INSERT) {
    using deque_type     = typename TestFixture::deque_type;
    using iterator       = typename TestFixture::iterator;
    deque_type x(100);
    for(int i = 0; i < 100; i++) {
        x[i] = i;
    }
    iterator b = x.begin();
    x.insert(b,-1);
    EXPECT_TRUE(x.size() == 101);
}

TYPED_TEST(DequeFixture, test3_for_deque_INSERT) {
    using deque_type     = typename TestFixture::deque_type;
    using iterator       = typename TestFixture::iterator;
    deque_type x(100);
    for(int i = 0; i < 100; i++) {
        x[i] = i;
    }
    iterator b = x.begin();
    x.insert(b,-1);
    bool pass = true;
    for(int i = 0; i < 11; i++) {
        if(x[i] != i-1)
            pass = false;
    }
    EXPECT_TRUE(pass == true);
}

TYPED_TEST(DequeFixture, test4_for_deque_INSERT) {
    using deque_type     = typename TestFixture::deque_type;
    using iterator       = typename TestFixture::iterator;
    deque_type x(100);
    for(int i = 0; i < 100; i++) {
        x[i] = i;
    }
    iterator b = x.begin();
    iterator b2 = x.insert(b,-1);
    EXPECT_TRUE(b2 == x.begin());
}

/*
 * TESTING 'ERASE' FOR DEQUE
 */
TYPED_TEST(DequeFixture, test1_for_deque_ERASE) {
    using deque_type     = typename TestFixture::deque_type;
    using iterator       = typename TestFixture::iterator;
    deque_type x(100);
    for(int i = 0; i < 100; i++) {
        x[i] = i;
    }
    iterator b = x.begin();
    x.erase(b);
    EXPECT_TRUE(x.front() == 1);
}

TYPED_TEST(DequeFixture, test2_for_deque_ERASE) {
    using deque_type     = typename TestFixture::deque_type;
    using iterator       = typename TestFixture::iterator;
    deque_type x(100);
    for(int i = 0; i < 100; i++) {
        x[i] = i;
    }
    iterator b = x.begin();
    x.erase(b);
    EXPECT_TRUE(x.size() == 99);
}

TYPED_TEST(DequeFixture, test3_for_deque_ERASE) {
    using deque_type     = typename TestFixture::deque_type;
    using iterator       = typename TestFixture::iterator;
    deque_type x(100);
    for(int i = 0; i < 100; i++) {
        x[i] = i;
    }
    iterator b = x.begin();
    iterator b2 = x.erase(b);
    EXPECT_TRUE(b2 == x.begin());
}

/*
 * TESTING 'POP_FRONT' FOR DEQUE
 */
TYPED_TEST(DequeFixture, test_for_deque_POPFRONT) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x(100);
    for(int i = 0; i < 100; i++) {
        x[i] = i;
    }
    bool pass = true;
    for(int i = 0; i < 100; i++) {
        if(x.front() != i)
            pass = false;
        x.pop_front();
    }
    EXPECT_TRUE(pass == true);
}

/*
 * TESTING 'POP_BACK' FOR DEQUE
 */
TYPED_TEST(DequeFixture, test_for_deque_POPBACK) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x(100);
    for(int i = 0; i < 100; i++) {
        x[i] = i;
    }
    bool pass = true;
    for(int i = 99; i >= 0; i--) {
        if(x.back() != i)
            pass = false;
        x.pop_back();
    }
    EXPECT_TRUE(pass == true);
}

/*
 * TESTING 'PUSH_FRONT' FOR DEQUE
 */
TYPED_TEST(DequeFixture, test_for_deque_PUSHFRONT) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x(100);
    for(int i = 0; i < 100; i++) {
        x.push_front(i);
    }
    //99, 98 , ...

    bool pass = true;
    for(int i = 99; i >= 0; i--) {
        if(x[99-i] != i)
            pass = false;
    }
    EXPECT_TRUE(pass == true);
}

// /*
//  * TESTING 'PUSH_BACK' FOR DEQUE
//  */
TYPED_TEST(DequeFixture, test_for_deque_PUSHBACK) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type x(100);
    for(int i = 0; i < 100; i++) {
        x.push_back(i);
    }
    //0, 1, 2, ...

    bool pass = true;
    for(int i = 99; i >= 0; i--) {
        if(x.back() != i)
            pass = false;
        x.pop_back();
    }
    EXPECT_TRUE(pass == true);
}

/*
 * TESTING '<' FOR DEQUE
 */
TYPED_TEST(DequeFixture, test1_for_deque_LESSTHAN) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type a({0,1,2,3});
    deque_type b({0,1,2,3,4});
    EXPECT_TRUE(a < b);
}

TYPED_TEST(DequeFixture, test2_for_deque_LESSTHAN) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type a({0,1,2,3});
    deque_type b({0,1,2,3});
    EXPECT_FALSE(a < b);
}

TYPED_TEST(DequeFixture, test3_for_deque_LESSTHAN) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type a({0,1,2,3});
    deque_type b({0,1,4,3});
    EXPECT_TRUE(a < b);
}

/*
 * TESTING 'RESIZE' FOR DEQUE
 */
TYPED_TEST(DequeFixture, test1_for_deque_RESIZE) {
    using deque_type     = typename TestFixture::deque_type;
    using iterator       = typename TestFixture::iterator;
    deque_type a({0,1,2,3});
    a.resize(2);
    bool pass = true;
    iterator it = a.begin();
    int i = 0;
    while(it != a.end()) {
        if(*it != i)
            pass = false;
        i++;
        it++;
    }
    EXPECT_TRUE(pass);
}

TYPED_TEST(DequeFixture, test2_for_deque_RESIZE) {
    using deque_type     = typename TestFixture::deque_type;
    using iterator       = typename TestFixture::iterator;
    deque_type a({0,1,2,3});
    a.resize(8);
    bool pass = true;
    iterator it = a.begin();
    int i = 0;
    while(it != a.end()) {
        if(*it != i && i<=3) {
            pass = false;
        } else if(i > 3 && *it != 0) {
            pass = false;
        }
        i++;
        it++;
    }
    EXPECT_TRUE(pass);
}

/*
 * TESTING 'CLEAR' FOR DEQUE
 */
TYPED_TEST(DequeFixture, test1_for_deque_CLEAR) {
    using deque_type     = typename TestFixture::deque_type;
    deque_type a({0,1,2,3,4,5,6});
    a.clear();
    EXPECT_TRUE(a.size() == 0 && a.begin() == a.end());
}

/*
 * TESTING '=' OPERATOR FOR DEQUE
 */
TYPED_TEST(DequeFixture, test1_for_deque_ASSIGNMENT_OPERATOR) {
    using deque_type     = typename TestFixture::deque_type;
    using iterator       = typename TestFixture::iterator;
    deque_type a({0,1,2,3});
    deque_type b({0,0,0});
    b = a;
    iterator beg = b.begin();
    bool pass = true;
    for(int i = 0; i <= 3; i++) {
        if(*beg != i)
            pass = false;
        beg++;
    }
    EXPECT_TRUE(pass);
}


TYPED_TEST(DequeFixture, test2_for_deque_ASSIGNMENT_OPERATOR) {
    using deque_type     = typename TestFixture::deque_type;
    using iterator       = typename TestFixture::iterator;
    deque_type a({0,1,2,3});
    a = a;
    iterator beg = a.begin();
    bool pass = true;
    for(int i = 0; i <= 3; i++) {
        if(*beg != i)
            pass = false;
        beg++;
    }
    EXPECT_TRUE(pass);
}

/*
 * TESTING 'SWAP' OPERATOR FOR DEQUE
 */
TYPED_TEST(DequeFixture, test_for_deque_SWAP) {
    using deque_type     = typename TestFixture::deque_type;
    using iterator       = typename TestFixture::iterator;
    deque_type a({0,1,2,3});
    deque_type b({0,0,0});
    a.swap(b);
    iterator beg = b.begin();
    int i = 0;
    bool pass = true;
    while(beg != b.end() && i < 4) {
        if(*beg != i)
            pass = false;
        beg++;
        i++;
    }
    iterator b2 = a.begin();
    i = 0;
    while(b2 != b.end() && i < 3) {
        if(*b2 != 0)
            pass = false;
        b2++;
        i++;
    }

    EXPECT_TRUE(pass);
}
